# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.1 - 2022-01-27
### Fixed
- Fix sign of transformed estimations following suggestion from notebook INSERT_LINK_NB, further 
  illustrated in section XYZ in the paper INSERT_LINK_PAPER_COMMIT

## 0.1.0 - 2020-12-29
### Added
- `TMCSwarm`.
- `TMCTransformationModel` has now the `evaluate()` method.

### Changed
- Internal computations are made more efficient by replacing `tf.map_fn(g, x)` with `g(x)`.

## 0.0.0 - 2020-12-23
### Added
- `TMCTransformationModel` class, that allows to optimize a transformation defined as a tensorflow model as prescribed in
  the [Transformer Monte
  Carlo
  method](https://gitlab.com/francesco-calcavecchia/deep-twin-sampling-monte-carlo-paper).
- Utilities, such as `compute_transformed_estimations`, `compute_tmc_estimations`, and `compute_tmc_loss`.
