from typing import Callable, Tuple

import tensorflow as tf

from transformer_monte_carlo.transformed_estimation_components.rho import (
    compute_rho_factors,
)
from transformer_monte_carlo.transformed_estimation_components.transform_coordinate import (
    compute_transformed_coordinates_and_jacobian,
)
from transformer_monte_carlo.verification import verify_tensor_is_rank_2

TransformedEstimations = tf.Tensor


def compute_transformed_estimations(
    model: tf.keras.Model,
    rho: Callable[[tf.Tensor], tf.Tensor],
    g: Callable[[tf.Tensor], tf.Tensor],
    x: tf.Tensor,
) -> TransformedEstimations:
    """
    Compute transformed estimations, where the transformation is represented by a tensorflow model.

    Args:
        model: Transformation described by a tensorflow model.
        rho: Probability density function of the Monte Carlo integral.
        g: Estimator function.
        x: Sampled coordinates.

    Returns:
        Transformed estimations, as prescribed for the [Transformer Monte Carlo method](https://gitlab.com/francesco-calcavecchia/deep-twin-sampling-monte-carlo-paper).
    """
    verify_tensor_is_rank_2(t=x)

    t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=x)
    rho_factors = compute_rho_factors(rho=rho, x=x, t=t)
    g_of_t = g(t)

    determinant_jacobian_matrix = tf.linalg.det(jacobian)

    sign = tf.math.sign(tf.reduce_sum(tf.reshape(rho_factors * determinant_jacobian_matrix, (x.shape[0], 1))))
    transformed_estimations = sign * tf.multiply(
        tf.reshape(rho_factors * determinant_jacobian_matrix, (x.shape[0], 1)), g_of_t
    )

    return transformed_estimations
