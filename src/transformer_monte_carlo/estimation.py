import tensorflow as tf

from transformer_monte_carlo.transformed_estimation import TransformedEstimations
from transformer_monte_carlo.verification import (
    verify_two_tensors_are_rank_2_and_same_shape,
)

TMCEstimations = tf.Tensor


def compute_tmc_estimations(transformed_estimations: TransformedEstimations, g_values: tf.Tensor) -> TMCEstimations:
    """
    Compute the Transformer Monte Carlo estimations. Currently supports only one transformation, and weights are all equal and set to 0.5.

    Args:
        transformed_estimations: Transformed estimations, as obtained from [`compute_transformed_estimations`][transformer_monte_carlo.transformed_estimation.compute_transformed_estimations])
        g_values: Values of the estimator `g`, computed on the sampled coordinates.

    Returns:
        Transformer Monte Carlo estimations, where `alpha_0` = `alpha_1` = `0.5`.

    """
    verify_two_tensors_are_rank_2_and_same_shape(
        t1=transformed_estimations,
        t2=g_values,
        name1="transformed_estimations",
        name2="g_values",
    )

    return 0.5 * (transformed_estimations + g_values)
