import tensorflow as tf

from transformer_monte_carlo.estimation import TMCEstimations
from transformer_monte_carlo.verification import verify_tensor_is_rank_2


def compute_tmc_loss(tmc_estimations: TMCEstimations) -> tf.Tensor:
    """
    Compute Transformed Monte Carlo (TMC) loss, defined as the standard deviation of the TMC estimations.

    Args:
        tmc_estimations: Transformed Monte Carlo estimations, as obtained from [`compute_tmc_estimations`][transformer_monte_carlo.estimation.compute_tmc_estimations]

    Returns:
        Transformer Monte Carlo loss, defined as the standard deviation of the Transformed Monte Carlo estimations.
    """
    verify_tensor_is_rank_2(t=tmc_estimations, name="tmc_estimations")

    return tf.reduce_sum(tf.nn.moments(tmc_estimations, axes=[0])[1])
