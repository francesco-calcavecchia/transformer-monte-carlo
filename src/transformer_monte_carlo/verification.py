import tensorflow as tf


@tf.function
def verify_two_tensors_are_rank_2_and_same_shape(
    t1: tf.Tensor, t2: tf.Tensor, name1: str = "tensor1", name2: str = "tensor2"
):
    verify_tensor_is_rank_2(t=t1, name=name1)
    verify_tensor_is_rank_2(t=t2, name=name2)
    verify_two_tensors_have_the_same_shape(t1=t1, t2=t2, name1=name1, name2=name2)


@tf.function
def verify_tensor_is_rank_2(t: tf.Tensor, name: str = "tensor"):
    if len(t.shape) != 2:
        raise ValueError(f"{name} should be of rank 2 but instead has shape {t.shape} " f"(rank={len(t.shape)})")


@tf.function
def verify_two_tensors_have_the_same_shape(
    t1: tf.Tensor, t2: tf.Tensor, name1: str = "tensor1", name2: str = "tensor2"
):
    if t1.shape != t2.shape:
        raise ValueError(
            f"{name1} and {name2} should have the same shape " f"but found {t1.shape[0]} and {t2.shape[0]}"
        )
