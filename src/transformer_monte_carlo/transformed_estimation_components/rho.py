from typing import Callable
import tensorflow as tf

from transformer_monte_carlo.verification import (
    verify_two_tensors_are_rank_2_and_same_shape,
)

RhoFactors = tf.Tensor


def compute_rho_factors(rho: Callable[[tf.Tensor], tf.Tensor], x: tf.Tensor, t: tf.Tensor) -> RhoFactors:
    verify_two_tensors_are_rank_2_and_same_shape(t1=x, t2=t, name1="x", name2="t")
    return tf.reshape(rho(t) / rho(x), (x.shape[0]))
