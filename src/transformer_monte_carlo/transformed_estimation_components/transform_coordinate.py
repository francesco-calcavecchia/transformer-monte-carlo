from typing import Tuple

import tensorflow as tf

from transformer_monte_carlo.verification import verify_tensor_is_rank_2

TransformedCoordinates = tf.Tensor
TransformationJacobian = tf.Tensor


@tf.function
def compute_transformed_coordinates_and_jacobian(
    model: tf.keras.Model, x: tf.Tensor
) -> Tuple[TransformedCoordinates, TransformationJacobian]:
    verify_tensor_is_rank_2(t=x)

    with tf.GradientTape() as tape_dx:
        tape_dx.watch(x)
        t = model(x)

    return t, tape_dx.batch_jacobian(t, x)
