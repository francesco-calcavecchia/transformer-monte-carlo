from typing import List, Callable

import tensorflow as tf

from transformer_monte_carlo.estimation import compute_tmc_estimations
from transformer_monte_carlo.loss import compute_tmc_loss
from transformer_monte_carlo.transformed_estimation import (
    compute_transformed_estimations,
)


class TMCTransformationModel:
    """
    Transformer Monte Carlo transformation model built on top of a tensorflow model.
    """

    def __init__(
        self,
        model: tf.keras.Model,
        rho: Callable[[tf.Tensor], tf.Tensor],
        g: Callable[[tf.Tensor], tf.Tensor],
    ):
        """
        Args:
            model: Transformation described by a tensorflow model.
            rho: Probability density function of the Monte Carlo integral.
            g: Estimator function.
        """
        self.model = model
        self.rho = rho
        self.g = g

    def get_model_parameters(self) -> List[tf.Variable]:
        """
        Return the tensorflow model parameters.

        Returns:
            List of tensorflow model parameters.
        """
        return [variable for layer in self.model.layers for variable in layer.variables]

    def optimize(
        self,
        x: tf.Tensor,
        g_values: tf.Tensor,
        optimizer: tf.keras.optimizers.Optimizer = tf.keras.optimizers.Adam(),
        batch_size: int = 100,
        epochs: int = 1,
    ):
        """
        Optimize the model in order to reduce the TMC loss.

        Args:
            x: Sampled coordinates.
            g_values: Estimations corresponding to the sampled coordinates.
            optimizer: Optimizer instance. See [`tf.keras.optimizers`](https://www.tensorflow.org/api_docs/python/tf/keras/optimizers).
            batch_size: Number of samples per gradient update.
            epochs: Number of epochs to train the model. An epoch is an iteration over the entire `x` and `g_values` data provided.

        Returns:

        """
        dataset = tf.data.Dataset.zip(
            (
                tf.data.Dataset.from_tensor_slices(x),
                tf.data.Dataset.from_tensor_slices(g_values),
            )
        )
        model_parameters = self.get_model_parameters()

        for epoch in range(epochs):
            dataset.shuffle(buffer_size=x.shape[0])
            for x_batch, g_values_batch in dataset.batch(batch_size, drop_remainder=False):
                with tf.GradientTape() as model_parameters_tape:
                    model_parameters_tape.watch(model_parameters)
                    loss = self.evaluate(x=x_batch, g_values=g_values_batch)
                gradient = model_parameters_tape.gradient(loss, model_parameters)
                optimizer.apply_gradients(zip(gradient, model_parameters))

    def evaluate(self, x: tf.Tensor, g_values: tf.Tensor) -> tf.Tensor:
        """
        Evaluate the model.

        Args:
            x: Sampled coordinates.
            g_values: Estimations corresponding to the sampled coordinates.

        Returns:
            The loss.
        """
        transformed_estimations = compute_transformed_estimations(model=self.model, rho=self.rho, g=self.g, x=x)
        tmc_estimations = compute_tmc_estimations(
            transformed_estimations=transformed_estimations,
            g_values=g_values,
        )
        return compute_tmc_loss(tmc_estimations=tmc_estimations)
