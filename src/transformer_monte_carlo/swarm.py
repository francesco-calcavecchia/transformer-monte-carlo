import warnings
from typing import Iterable, Callable, List

import tensorflow as tf

from transformer_monte_carlo.tf_control.warning import with_muted_tf_warnings
from transformer_monte_carlo.transformation_model import TMCTransformationModel


class TMCSwarm:
    """
    Swarm of `TMCTransformationModel`s. This class can be useful if you want to use a simple genetic algorithm to find
    the optimal transformation model. In fact, you can start with a large number of tmc_models in a swarm, and the optimize
    all of them, prune to reduce the tmc_models to a smaller number, then optimize further, prune again, etc. until you
    eventually end up with a single optimized model.
    """

    def __init__(
        self,
        models: Iterable[tf.keras.Model],
        rho: Callable[[tf.Tensor], tf.Tensor],
        g: Callable[[tf.Tensor], tf.Tensor],
    ):
        """

        Args:
            models: Iterable of transformations described by a tensorflow model.
            rho: Probability density function of the Monte Carlo integral.
            g: Estimator function.
        """
        self.tmc_models = [TMCTransformationModel(model=model, rho=rho, g=g) for model in models]
        self.rho = rho
        self.g = g

    @with_muted_tf_warnings
    def optimize(
        self,
        x: tf.Tensor,
        g_values: tf.Tensor,
        optimizer: tf.keras.optimizers.Optimizer = tf.keras.optimizers.Adam(),
        batch_size: int = 100,
        epochs: int = 1,
        mute_tf_warnings: bool = True,
    ):
        """
        Optimize all the tmc_models in the swarm.
        If a model raises an exception during the optimization, the model will be removed from the swarm with a warning message.

        Args:
            x: Sampled coordinates.
            g_values: Estimations corresponding to the sampled coordinates.
            optimizer: Optimizer instance. See [`tf.keras.optimizers`](https://www.tensorflow.org/api_docs/python/tf/keras/optimizers).
            batch_size: Number of samples per gradient update.
            epochs: Number of epochs to train the model. An epoch is an iteration over the entire `x` and `g_values` data provided.
            mute_tf_warnings: Display tensorflow log warnings? Note that currently it is known that if the swarm contains more than 5 models tf will send performance warnings because of excessive number of tf.function retracing that can spam your output. It is currently not clear if this can be avoided (probably not) so we suggest to mute these warnings.


        Returns:

        """
        models_to_remove = list()

        for i, model in enumerate(self.tmc_models):
            try:
                model.optimize(
                    x=x,
                    g_values=g_values,
                    optimizer=optimizer,
                    batch_size=batch_size,
                    epochs=epochs,
                )
            except Exception as e:
                models_to_remove.append(i)
                warnings.warn(
                    f"Model {i} raised an exception during the optimization and will therefore be removed from the swarm. "
                    f"The error was: {e}"
                )

        self.tmc_models = [m for i, m in enumerate(self.tmc_models) if i not in models_to_remove]

    @with_muted_tf_warnings
    def evaluate(self, x: tf.Tensor, g_values: tf.Tensor, mute_tf_warnings: bool = True) -> List[tf.Tensor]:
        """

        Args:
            x: Sampled coordinates.
            g_values: Estimations corresponding to the sampled coordinates.
            mute_tf_warnings: Display tensorflow log warnings? Note that currently it is known that if the swarm contains more than 5 models tf will send performance warnings because of excessive number of tf.function retracing that can spam your output. It is currently not clear if this can be avoided (probably not) so we suggest to mute these warnings.


        Returns:
            The loss of each model in the swarm.
        """
        return [model.evaluate(x=x, g_values=g_values) for model in self.tmc_models]

    @with_muted_tf_warnings
    def prune(
        self,
        n_models: int,
        x: tf.Tensor,
        g_values: tf.Tensor,
        mute_tf_warnings: bool = True,
    ):
        """
        Prune the swarm, by evaluating all the tmc_models and keeping the `n_models` with the best score (lowest loss).

        Args:
            n_models: Number of tmc_models that the swarm should have after pruning.
            x: Sampled coordinates.
            g_values: Estimations corresponding to the sampled coordinates.
            mute_tf_warnings: Display tensorflow log warnings? Note that currently it is known that if the swarm contains more than 5 models tf will send performance warnings because of excessive number of tf.function retracing that can spam your output. It is currently not clear if this can be avoided (probably not) so we suggest to mute these warnings.

        Returns:

        """
        if n_models <= 0:
            raise ValueError("Called TMCSwarm.prune() with n_models <= 0.")
        elif n_models > len(self.tmc_models):
            warnings.warn(
                "Called TMCSwarm.prune() with n_models larger than the tmc_models currently in the swarm. "
                "Prune will have no effect.",
                category=RuntimeWarning,
            )
        else:
            self._do_prune(n_models=n_models, x=x, g_values=g_values)

    def _do_prune(self, n_models: int, x: tf.Tensor, g_values: tf.Tensor):
        model_losses = [model.evaluate(x=x, g_values=g_values) for model in self.tmc_models]
        pruned_model_indices = tf.argsort(model_losses, direction="ASCENDING", stable=True)[0:n_models]
        self.tmc_models = [self.tmc_models[i] for i in pruned_model_indices]
