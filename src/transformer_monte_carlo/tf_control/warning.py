from functools import wraps
from typing import Callable
import tensorflow as tf


def with_muted_tf_warnings(f: Callable) -> Callable:
    @wraps(f)
    def muted_f(*args, **kwargs):
        original_log_level = tf.get_logger().level
        if kwargs.get("mute_tf_warnings", True):
            tf.get_logger().setLevel("ERROR")

        result = f(*args, **kwargs)

        if kwargs.get("mute_tf_warnings", True):
            tf.get_logger().setLevel(original_log_level)
        return result

    return muted_f
