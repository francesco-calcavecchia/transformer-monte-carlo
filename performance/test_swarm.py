from datetime import datetime
from unittest import TestCase

import tensorflow as tf
import tensorflow_probability as tfp

from transformer_monte_carlo.swarm import TMCSwarm
from transformer_monte_carlo.transformation_model import TMCTransformationModel

rho_pdf = tfp.distributions.Normal(loc=0.0, scale=1.0)


@tf.function
def rho(x: tf.Tensor) -> tf.Tensor:
    return rho_pdf.prob(x)


@tf.function
def g(x: tf.Tensor) -> tf.Tensor:
    return tf.cos(tf.constant(20.0) * x)


def get_ffnn(size=15):
    return tf.keras.models.Sequential(
        [
            tf.keras.layers.Dense(units=size, input_shape=(1,), activation=tf.keras.activations.tanh),
            tf.keras.layers.Dense(units=1, activation=tf.keras.activations.tanh),
            tf.keras.layers.Dense(units=1, activation=tf.keras.activations.linear),
        ]
    )


class TestSwarm(TestCase):
    def setUp(self) -> None:
        Nmc = 10000
        x = rho_pdf.sample((Nmc, 1))
        g_values = g(x)
        self.x_train, self.x_test = tf.split(x, [Nmc * 8 // 10, Nmc * 2 // 10])
        self.g_values_train, self.g_values_test = tf.split(g_values, [Nmc * 8 // 10, Nmc * 2 // 10])

        tf.random.set_seed(42)
        self.swarm = TMCSwarm(models=[get_ffnn(size=3) for _ in range(10)], rho=rho, g=g)

    def test_optimize(self):
        time_0 = datetime.now()
        self.swarm.optimize(
            x=self.x_train,
            g_values=self.g_values_train,
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.025),
            batch_size=1000,
            epochs=1,
        )
        time_1 = datetime.now()
        print("optimize swarm = ", time_1 - time_0)
        print((time_1 - time_0).total_seconds())
        self.assertLess((time_1 - time_0).total_seconds(), 10.0)

    def test_prune(self):
        time_0 = datetime.now()
        self.swarm.prune(
            n_models=9,
            x=self.x_test,
            g_values=self.g_values_test,
            mute_tf_warnings=True,
        )
        time_1 = datetime.now()
        print("prune swarm = ", time_1 - time_0)
        print((time_1 - time_0).total_seconds())
        self.assertLess((time_1 - time_0).total_seconds(), 4.0)
