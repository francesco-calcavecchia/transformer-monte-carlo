# Transformer Monte Carlo

Convenience tools built on top of tensorflow for the [Transformer Monte Carlo method](https://gitlab.com/francesco-calcavecchia/deep-twin-sampling-monte-carlo-paper).
