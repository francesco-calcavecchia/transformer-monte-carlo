# Transformer Monte Carlo

[Documentation page](https://francesco-calcavecchia.gitlab.io/transformer-monte-carlo/)

Convenience tools built on top of tensorflow for
the [Transformer Monte Carlo method](https://gitlab.com/francesco-calcavecchia/deep-twin-sampling-monte-carlo-paper).

![logo](docs/img/transformer-monte-carlo.jpg)

## For the developer

* Create a virtual environment and activate it (if you have [pyenv](https://github.com/pyenv/pyenv) installed the 
  version of python will be automatically set, otherwise refer to the file `.python-version`)
  ```shell
  python -m venv venv
  source venv/bin/activate
  ```

* Install the developer dependencies you will need
  ```shell
  pip install -U pip wheel setuptools
  pip install -e .[dev,test,doc]
  ```
  
* Set black as pre-commit package (will automatically apply [black](https://github.com/psf/black) before committing)
  ```shell
  pre-commit install
  ```
  
* To run the tests
  ```shell
  python -m unittest discover test
  ```
  
* To build the documentation and navigate it
  ```shell
  mkdocs serve
  ```