import logging
from unittest import TestCase
from unittest.mock import MagicMock

import tensorflow as tf
import tensorflow_probability as tfp

from test.sample_input import get_one_dimensional_linear_model
from transformer_monte_carlo.swarm import TMCSwarm


class TestTMCModelSwarm(TestCase):
    def setUp(self) -> None:
        tf.random.set_seed(42)
        self.rho_pdf = tfp.distributions.Normal(loc=0.0, scale=1.0)

        @tf.function
        def rho(x: tf.Tensor) -> tf.Tensor:
            return self.rho_pdf.prob(x)

        self.rho = rho

        @tf.function
        def g(x: tf.Tensor) -> tf.Tensor:
            return tf.cos(tf.constant(20.0) * x)

        self.g = g
        self.x = self.rho_pdf.sample((100, 1))
        self.g_values = self.g(self.x)

        self.nn1 = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(units=3, activation="sigmoid", input_shape=(1,)),
                tf.keras.layers.Dense(units=1, activation="linear"),
            ]
        )
        self.nn2 = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(units=3, activation="sigmoid", input_shape=(1,)),
                tf.keras.layers.Dense(units=1, activation="linear"),
            ]
        )
        self.nn3 = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(units=3, activation="sigmoid", input_shape=(1,)),
                tf.keras.layers.Dense(units=1, activation="linear"),
            ]
        )

    def test_if_optimize_then_all_single_models_are_optimized(self):
        tmc_swarm = TMCSwarm(models=[self.nn1, self.nn2], rho=self.rho, g=self.g)

        tmc_loss_before_optimization1 = tmc_swarm.tmc_models[0].evaluate(x=self.x, g_values=self.g_values)
        tmc_loss_before_optimization2 = tmc_swarm.tmc_models[1].evaluate(x=self.x, g_values=self.g_values)

        tmc_swarm.optimize(x=self.x, g_values=self.g_values, batch_size=100, epochs=1)

        tmc_loss_after_optimization1 = tmc_swarm.tmc_models[0].evaluate(x=self.x, g_values=self.g_values)
        tmc_loss_after_optimization2 = tmc_swarm.tmc_models[1].evaluate(x=self.x, g_values=self.g_values)

        self.assertLess(tmc_loss_after_optimization1.numpy(), tmc_loss_before_optimization1.numpy())
        self.assertLess(tmc_loss_after_optimization2.numpy(), tmc_loss_before_optimization2.numpy())

    def test_if_optimize_has_one_model_failing_then_remove_the_model_from_the_swarm(
        self,
    ):
        tmc_swarm = TMCSwarm(models=[self.nn1, self.nn2], rho=self.rho, g=self.g)
        tmc_swarm.tmc_models[0].optimize = MagicMock(side_effect=Exception())
        tmc_swarm.optimize(x=self.x, g_values=self.g_values, batch_size=100, epochs=1)
        self.assertEqual(1, len(tmc_swarm.tmc_models))
        self.assertIs(self.nn2, tmc_swarm.tmc_models[0].model)

    def test_if_prune_to_one_then_only_model_with_lowest_loss_is_kept(self):
        tmc_swarm = TMCSwarm(models=[self.nn1, self.nn2, self.nn3], rho=self.rho, g=self.g)
        tmc_loss_nn1 = tmc_swarm.tmc_models[0].evaluate(x=self.x, g_values=self.g_values)
        tmc_loss_nn2 = tmc_swarm.tmc_models[1].evaluate(x=self.x, g_values=self.g_values)
        tmc_loss_nn3 = tmc_swarm.tmc_models[2].evaluate(x=self.x, g_values=self.g_values)

        tmc_swarm.prune(n_models=1, x=self.x, g_values=self.g_values)

        self.assertEqual(1, len(tmc_swarm.tmc_models))
        self.assertEqual(
            tf.reduce_min(tf.concat([tmc_loss_nn1, tmc_loss_nn2, tmc_loss_nn3], axis=0)).numpy(),
            tmc_swarm.tmc_models[0].evaluate(x=self.x, g_values=self.g_values).numpy(),
        )

    def test_if_prune_with_n_models_larger_than_the_number_of_models_in_the_swarm_then_a_warning_is_raised(
        self,
    ):
        tmc_swarm = TMCSwarm(models=[self.nn1], rho=self.rho, g=self.g)
        with self.assertWarns(RuntimeWarning):
            tmc_swarm.prune(n_models=2, x=self.x, g_values=self.g_values)

    def test_if_prune_with_n_models_equal_or_smaller_than_the_number_of_models_in_the_swarm_then_no_warning(
        self,
    ):
        tmc_swarm = TMCSwarm(models=[self.nn1, self.nn2], rho=self.rho, g=self.g)
        tmc_swarm.prune(n_models=2, x=self.x, g_values=self.g_values)
        tmc_swarm.prune(n_models=1, x=self.x, g_values=self.g_values)

    def test_if_prune_with_n_models_negative_or_equal_zero_then_an_exception_is_raised(
        self,
    ):
        tmc_swarm = TMCSwarm(models=[self.nn1], rho=self.rho, g=self.g)
        with self.assertRaises(ValueError):
            tmc_swarm.prune(n_models=0, x=self.x, g_values=self.g_values)
        with self.assertRaises(ValueError):
            tmc_swarm.prune(n_models=-1, x=self.x, g_values=self.g_values)
        with self.assertRaises(ValueError):
            tmc_swarm.prune(n_models=-32, x=self.x, g_values=self.g_values)

    def test_if_prune_with_multiple_models_and_mute_true_then_no_tf_warnings(self):
        tmc_swarm = TMCSwarm(
            models=[get_one_dimensional_linear_model(m=float(m + 1), q=0.0) for m in range(10)],
            rho=self.rho,
            g=self.g,
        )
        with self.assertRaises(AssertionError):
            with self.assertLogs("tensorflow", level="WARNING"):
                tmc_swarm.prune(n_models=1, x=self.x, g_values=self.g_values, mute_tf_warnings=True)

    def test_if_evaluate_is_called_then_return_losses(self):
        tmc_swarm = TMCSwarm(models=[self.nn1, self.nn2], rho=self.rho, g=self.g)
        evaluations = tmc_swarm.evaluate(x=self.x, g_values=self.g_values)
        for i in range(2):
            self.assertEqual(
                tmc_swarm.tmc_models[i].evaluate(x=self.x, g_values=self.g_values).numpy(),
                evaluations[i].numpy(),
            )
