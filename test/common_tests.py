from abc import ABC, abstractmethod

import tensorflow as tf


class InputIsTensorRank2Mixin(ABC):
    def test_if_input_is_not_rank_2_then_raise_error(self):
        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_is_rank_2_mixin(t=tf.convert_to_tensor([0.0, 1.0, -1.0]))

        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_is_rank_2_mixin(
                t=tf.convert_to_tensor([[[0.0]], [[1.0]], [[-1.0]]])
            )

    @abstractmethod
    def call_function_under_test_for_input_is_rank_2_mixin(self, t):
        pass

    @abstractmethod
    def assertRaises(self, expected_exception, *args, **kwargs):
        pass


class TwoInputTensorsRank2AndSameShapeMixin(ABC):
    def test_if_provided_inputs_are_not_rank_2_then_raise_error(self):
        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(
                t1=tf.convert_to_tensor([0.5, 1.0, 2.0]),
                t2=tf.convert_to_tensor([[0.25], [-2.0], [0.5]]),
            )

        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(
                t1=tf.convert_to_tensor([[[0.5]], [[1.0]], [[2.0]]]),
                t2=tf.convert_to_tensor([[0.25], [-2.0], [0.5]]),
            )

        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(
                t1=tf.convert_to_tensor([[0.5], [1.0], [2.0]]),
                t2=tf.convert_to_tensor([0.25, -2.0, 0.5]),
            )

        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(
                t1=tf.convert_to_tensor([[0.5], [1.0], [2.0]]),
                t2=tf.convert_to_tensor([[[0.25]], [[-2.0]], [[0.5]]]),
            )

    def test_if_provided_inputs_have_different_size_then_raise_error(self):
        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(
                t1=tf.convert_to_tensor([[0.5, 1.0], [2.0, -1.0], [0.0, 0.1]]),
                t2=tf.convert_to_tensor([[0.25, 1.25], [-2.0, 1.5]]),
            )

    def test_if_provided_inputs_elements_have_different_dimensionality_then_raise_error(
        self,
    ):
        with self.assertRaises(ValueError):
            self.call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(
                t1=tf.convert_to_tensor([[0.5, 1.0], [2.0, -1.0], [0.0, 0.1]]),
                t2=tf.convert_to_tensor([[0.25], [-2.0], [0.5]]),
            )

    @abstractmethod
    def call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(self, t1: tf.Tensor, t2: tf.Tensor):
        pass

    @abstractmethod
    def assertRaises(self, expected_exception, *args, **kwargs):
        pass
