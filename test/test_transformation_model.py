from unittest import TestCase

import tensorflow as tf
import tensorflow_probability as tfp
from numpy.testing import assert_array_equal

from transformer_monte_carlo.estimation import compute_tmc_estimations
from transformer_monte_carlo.loss import compute_tmc_loss
from transformer_monte_carlo.transformation_model import TMCTransformationModel
from transformer_monte_carlo.transformed_estimation import (
    compute_transformed_estimations,
)


class TestTMCModel(TestCase):
    def setUp(self) -> None:
        tf.random.set_seed(42)
        self.rho_pdf = tfp.distributions.Normal(loc=0.0, scale=1.0)

        @tf.function
        def rho(x: tf.Tensor) -> tf.Tensor:
            return self.rho_pdf.prob(x)

        self.rho = rho

        @tf.function
        def g(x: tf.Tensor) -> tf.Tensor:
            return tf.cos(tf.constant(20.0) * x)

        self.g = g

        self.x = x = self.rho_pdf.sample((100, 1))
        self.g_values = self.g(x)

    def test_if_nn_has_no_layers_then_return_empty_list(self):
        tmc_model = TMCTransformationModel(model=tf.keras.Sequential(), rho=self.rho, g=self.g)
        self.assertCountEqual([], tmc_model.get_model_parameters())

    def test_if_nn_has_one_layer_then_return_list_with_its_variables(self):
        nn = tf.keras.Sequential([tf.keras.layers.Dense(units=1, activation="relu", input_shape=(1,))])
        tmc_model = TMCTransformationModel(model=nn, rho=self.rho, g=self.g)
        assert_array_equal(tmc_model.get_model_parameters()[0], nn.layers[0].variables[0].numpy())
        assert_array_equal(tmc_model.get_model_parameters()[1], nn.layers[0].variables[1].numpy())

    def test_if_nn_has_two_layers_then_return_list_with_its_variables(self):
        nn = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(units=1, activation="sigmoid", input_shape=(1,)),
                tf.keras.layers.Dense(units=3, activation="linear"),
            ]
        )
        tmc_model = TMCTransformationModel(model=nn, rho=self.rho, g=self.g)
        assert_array_equal(tmc_model.get_model_parameters()[0], nn.layers[0].variables[0].numpy())
        assert_array_equal(tmc_model.get_model_parameters()[1], nn.layers[0].variables[1].numpy())
        assert_array_equal(tmc_model.get_model_parameters()[2], nn.layers[1].variables[0].numpy())
        assert_array_equal(tmc_model.get_model_parameters()[3], nn.layers[1].variables[1].numpy())

    def test_if_optimize_then_loss_is_reduced(self):
        nn = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(units=3, activation="sigmoid", input_shape=(1,)),
                tf.keras.layers.Dense(units=1, activation="linear"),
            ]
        )
        tmc_model = TMCTransformationModel(model=nn, rho=self.rho, g=self.g)
        tmc_loss_before_optimization = self.__compute_loss(model=tmc_model.model, x=self.x, g_values=self.g_values)

        tmc_model.optimize(x=self.x, g_values=self.g_values, batch_size=100, epochs=1)
        tmc_loss_after_optimization = self.__compute_loss(model=tmc_model.model, x=self.x, g_values=self.g_values)

        self.assertLess(tmc_loss_after_optimization.numpy(), tmc_loss_before_optimization.numpy())

    def test_if_evaluate_is_called_then_return_loss(self):
        nn = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(units=3, activation="sigmoid", input_shape=(1,)),
                tf.keras.layers.Dense(units=1, activation="linear"),
            ]
        )
        tmc_model = TMCTransformationModel(model=nn, rho=self.rho, g=self.g)
        evaluation = tmc_model.evaluate(x=self.x, g_values=self.g_values)
        self.assertEqual(
            self.__compute_loss(model=tmc_model.model, x=self.x, g_values=self.g_values),
            evaluation.numpy(),
        )

    def __compute_loss(self, model: tf.keras.Model, x: tf.Tensor, g_values: tf.Tensor) -> tf.Tensor:
        return compute_tmc_loss(
            tmc_estimations=compute_tmc_estimations(
                transformed_estimations=compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=x),
                g_values=g_values,
            )
        )
