from unittest import TestCase

import numpy as np
import tensorflow as tf

from test.common_tests import TwoInputTensorsRank2AndSameShapeMixin
from transformer_monte_carlo.estimation import compute_tmc_estimations


class TestTMCEstimation(TestCase, TwoInputTensorsRank2AndSameShapeMixin):
    def call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(self, t1: tf.Tensor, t2: tf.Tensor):
        compute_tmc_estimations(transformed_estimations=t1, g_values=t2)

    def test_tmc_estimations_are_simple_average(self):
        tmc_estimations = compute_tmc_estimations(
            transformed_estimations=tf.convert_to_tensor([[0.5, 1.0], [2.0, -1.0], [0.0, 0.1]]),
            g_values=tf.convert_to_tensor([[0.25, 1.25], [-2.0, 1.5], [0.5, -0.1]]),
        )
        np.testing.assert_equal(
            tmc_estimations.numpy(),
            np.array([[0.375, 1.125], [0.0, 0.25], [0.25, 0.0]]),
        )
