from unittest.case import TestCase

import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from test.common_tests import TwoInputTensorsRank2AndSameShapeMixin
from transformer_monte_carlo.transformed_estimation_components.rho import (
    compute_rho_factors,
)


class TestComputeRhoFactors(TestCase, TwoInputTensorsRank2AndSameShapeMixin):
    def setUp(self) -> None:
        tf.random.set_seed(42)
        self.rho_pdf = tfp.distributions.Uniform(low=-1.0, high=1.0)

        @tf.function
        def rho(x: tf.Tensor) -> tf.Tensor:
            return self.rho_pdf.prob(x)

        self.rho = rho

        self.x = self.rho_pdf.sample((8, 1))
        self.g_fn = lambda x: x * x + 1.0
        self.g = self.g_fn(self.x)

    def call_function_under_test_for_input_tensors_are_rank_2_and_same_shape_mixin(self, t1: tf.Tensor, t2: tf.Tensor):
        return compute_rho_factors(rho=self.rho, x=t1, t=t2)

    def test_if_transformed_1d_coordinates_are_same_as_originals_then_factors_are_equal_to_one(
        self,
    ):
        rho_factors = compute_rho_factors(rho=self.rho, x=self.x, t=self.x)
        np.testing.assert_equal(rho_factors.numpy(), [1.0] * len(self.x))

    def test_if_transformed_1d_coordinates_are_such_that_rho_is_zero_then_factors_are_zero(
        self,
    ):
        rho_factors = compute_rho_factors(rho=self.rho, x=self.x, t=self.x + 10.0)
        np.testing.assert_equal(rho_factors.numpy(), [0.0] * len(self.x))

    def test_if_rho_evaluated_on_1d_coordinates_is_zero_then_factors_are_nan(self):
        @tf.function
        def rho(x):
            return tfp.distributions.Uniform(low=2.0, high=3.0).prob(x)

        rho_factors = compute_rho_factors(rho=rho, x=self.x, t=self.x)
        np.testing.assert_equal(rho_factors.numpy(), [np.nan] * len(self.x))

    def test_sample_case_1d(self):
        @tf.function
        def rho(x):
            return tfp.distributions.Triangular(low=0.0, peak=1.0, high=1.0).prob(x)

        rho_factors = compute_rho_factors(
            rho=rho,
            x=tf.convert_to_tensor([[0.0], [0.25], [0.5], [0.75]]),
            t=tf.convert_to_tensor([[0.0], [0.5], [1.0], [1.5]]),
        )
        np.testing.assert_equal(rho_factors.numpy(), [np.nan, 2.0, 2.0, 0.0])

    def test_sample_case_3d(self):
        @tf.function
        def rho(x):
            return tf.reduce_prod(
                tfp.distributions.Triangular(low=0.0, peak=1.0, high=1.0).prob(x),
                axis=1,
            )

        rho_factors = compute_rho_factors(
            rho=rho,
            x=tf.convert_to_tensor([[0.0, 0.0], [0.25, 0.5], [0.5, 0.75], [0.75, 0.0]]),
            t=tf.convert_to_tensor([[0.0, 0.0], [1.5, 0.25], [1.0, 0.5], [1.5, 2.0]]),
        )
        np.testing.assert_almost_equal(rho_factors.numpy(), [np.nan, 0.0, 4.0 / 3.0, np.nan], decimal=5)
