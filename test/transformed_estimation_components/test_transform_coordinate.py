from unittest import TestCase

import numpy as np
import tensorflow as tf

from test.common_tests import InputIsTensorRank2Mixin
from test.sample_input import (
    get_one_dimensional_linear_model,
    get_two_dimensional_linear_model,
)
from transformer_monte_carlo.transformed_estimation_components.transform_coordinate import (
    compute_transformed_coordinates_and_jacobian,
)


class TestComputeTransformedCoordinatesAndJacobian(TestCase, InputIsTensorRank2Mixin):
    def setUp(self) -> None:
        self.x = tf.convert_to_tensor([[0.0], [0.5], [1.0]])

    def call_function_under_test_for_input_is_rank_2_mixin(self, t):
        compute_transformed_coordinates_and_jacobian(model=get_one_dimensional_linear_model(m=1.0, q=0.0), x=t)

    def test_1d_model_is_zero(self):
        model = get_one_dimensional_linear_model(m=0.0, q=0.0)
        t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=self.x)
        np.testing.assert_equal(t.numpy(), [[0.0], [0.0], [0.0]])
        np.testing.assert_equal(jacobian.numpy(), [[[0.0]], [[0.0]], [[0.0]]])

    def test_1d_model_is_identity(self):
        model = get_one_dimensional_linear_model(m=1.0, q=0.0)
        t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=self.x)
        np.testing.assert_equal(t.numpy(), self.x.numpy())
        np.testing.assert_equal(jacobian.numpy(), [[[1.0]], [[1.0]], [[1.0]]])

    def test_1d_model_is_shift(self):
        model = get_one_dimensional_linear_model(m=1.0, q=1.0)
        t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=self.x)
        np.testing.assert_equal(t.numpy(), self.x.numpy() + 1.0)
        np.testing.assert_equal(jacobian.numpy(), [[[1.0]], [[1.0]], [[1.0]]])

    def test_1d_model_is_dilatation(self):
        model = get_one_dimensional_linear_model(m=2.0, q=0.0)
        t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=self.x)
        np.testing.assert_equal(t.numpy(), 2.0 * self.x.numpy())
        np.testing.assert_equal(jacobian.numpy(), [[[2.0]], [[2.0]], [[2.0]]])

    def test_1d_model_is_generic_linear_function(self):
        model = get_one_dimensional_linear_model(m=-2.0, q=1.5)
        t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=self.x)
        np.testing.assert_equal(t.numpy(), -2.0 * self.x.numpy() + 1.5)
        np.testing.assert_equal(jacobian.numpy(), [[[-2.0]], [[-2.0]], [[-2.0]]])

    def test_2d_model_is_generic_linear_function(self):
        model = get_two_dimensional_linear_model(m=-2.0, q=1.5)
        x = tf.convert_to_tensor([[0.0, 0.0], [0.5, 0.25], [1.0, 1.5]])
        t, jacobian = compute_transformed_coordinates_and_jacobian(model=model, x=x)
        np.testing.assert_equal(t.numpy(), -2.0 * x.numpy() + 1.5)
        np.testing.assert_equal(
            jacobian.numpy(),
            [
                [[-2.0, 0.0], [0.0, -2.0]],
                [[-2.0, 0.0], [0.0, -2.0]],
                [[-2.0, 0.0], [0.0, -2.0]],
            ],
        )
