from unittest import TestCase

import numpy as np
import tensorflow as tf

from test.common_tests import InputIsTensorRank2Mixin
from transformer_monte_carlo.loss import compute_tmc_loss


class TestTMCLoss(TestCase, InputIsTensorRank2Mixin):
    def call_function_under_test_for_input_is_rank_2_mixin(self, t):
        return compute_tmc_loss(tmc_estimations=t)

    def test_if_estimations_are_1d_then_loss_is_standard_deviation_of_tmc_estimations(
        self,
    ):
        loss = compute_tmc_loss(tmc_estimations=tf.convert_to_tensor([[0.0], [1.0], [-1.0]]))
        np.testing.assert_almost_equal(loss.numpy(), np.array([2.0 / 3.0]), decimal=5)

    def test_if_estimations_are_2d_then_loss_is_sum_ofstandard_deviation_of_tmc_estimations(
        self,
    ):
        loss = compute_tmc_loss(tmc_estimations=tf.convert_to_tensor([[0.0, 3.0], [1.0, 2.0], [-1.0, 4.0]]))
        np.testing.assert_almost_equal(loss.numpy(), np.array([4.0 / 3.0]), decimal=5)
