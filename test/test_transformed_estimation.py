from unittest import TestCase

import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from test.common_tests import InputIsTensorRank2Mixin
from test.sample_input import (
    get_one_dimensional_linear_model,
    get_two_dimensional_linear_model,
)
from transformer_monte_carlo.transformed_estimation import (
    compute_transformed_estimations,
)


class Test1DComputeTransformedEstimation(TestCase, InputIsTensorRank2Mixin):
    def setUp(self) -> None:
        tf.random.set_seed(42)
        self.rho_pdf = tfp.distributions.Uniform(low=-1.0, high=1.0)

        @tf.function
        def rho(x: tf.Tensor) -> tf.Tensor:
            return self.rho_pdf.prob(x)

        self.rho = rho
        self.x = self.rho_pdf.sample((8, 1))

        @tf.function
        def g(x: tf.Tensor) -> tf.Tensor:
            return x * x + 1.0

        self.g = g
        self.g_values = tf.map_fn(self.g, self.x)

    def call_function_under_test_for_input_is_rank_2_mixin(self, t):
        compute_transformed_estimations(
            model=get_one_dimensional_linear_model(m=1.0, q=0.0),
            rho=self.rho,
            g=self.g,
            x=t,
        )

    def test_if_1d_model_is_identity_then_transformed_estimations_are_same_as_original(
        self,
    ):
        model = get_one_dimensional_linear_model(m=1.0, q=0.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), self.g_values.numpy())

    def test_if_1d_model_is_minus_identity_then_transformed_estimations_are_same_as_original(self):
        model = get_one_dimensional_linear_model(m=-1.0, q=0.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), self.g_values.numpy())

    def test_if_1d_model_is_shift_outside_original_sample_range_then_transformed_estimations_are_zero(
        self,
    ):
        model = get_one_dimensional_linear_model(m=1.0, q=5.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), [[0.0]] * self.x.shape[0])

    def test_if_1d_model_is_zero_then_transformed_estimations_are_zero(self):
        model = get_one_dimensional_linear_model(m=0.0, q=0.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), [[0.0]] * self.x.shape[0])

    def test_if_1d_model_is_not_trivial_then_transformed_estimations_are_computed_correctly(
        self,
    ):
        model = get_one_dimensional_linear_model(m=-1.5, q=0.5)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)

        expected_rho_factor = tf.reshape(
            tf.map_fn(self.rho, model(self.x)) / tf.map_fn(self.rho, self.x),
            (self.x.shape[0]),
        )
        expected_sign = tf.math.sign(tf.reduce_sum(tf.reshape(expected_rho_factor * (-1.5), (self.x.shape[0], 1))))
        expected_g_t = expected_sign * tf.multiply(
            tf.reshape(expected_rho_factor * (-1.5), (self.x.shape[0], 1)),
            tf.map_fn(self.g, model(self.x)),
        )
        np.testing.assert_equal(transformed_estimations.numpy(), expected_g_t.numpy())


class Test2DComputeTransformedEstimation(TestCase):
    def setUp(self) -> None:
        tf.random.set_seed(42)
        self.rho_pdf = tfp.distributions.Uniform(low=-1.0, high=1.0)

        @tf.function
        def rho(x: tf.Tensor) -> tf.Tensor:
            return tf.reduce_prod(self.rho_pdf.prob(x), axis=1)

        self.rho = rho
        self.x = self.rho_pdf.sample((8, 2))

        @tf.function
        def g(x: tf.Tensor) -> tf.Tensor:
            return tf.reshape(tf.reduce_sum(x * x, axis=1) + 1.0, (x.shape[0], 1))

        self.g = g
        self.g_values = self.g(self.x)

    def test_if_2d_model_is_identity_then_transformed_estimations_are_same_as_original(
        self,
    ):
        model = get_two_dimensional_linear_model(m=1.0, q=0.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), self.g_values.numpy())

    def test_if_2d_model_is_shift_outside_original_sample_range_then_transformed_estimations_are_zero(
        self,
    ):
        model = get_two_dimensional_linear_model(m=1.0, q=5.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), [[0.0]] * self.x.shape[0])

    def test_if_2d_model_is_zero_then_transformed_estimations_are_zero(self):
        model = get_two_dimensional_linear_model(m=0.0, q=0.0)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)
        np.testing.assert_equal(transformed_estimations.numpy(), [[0.0]] * self.x.shape[0])

    def test_if_2d_model_is_not_trivial_then_transformed_estimations_are_computed_correctly(
        self,
    ):
        model = get_two_dimensional_linear_model(m=-1.5, q=0.5)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)

        expected_rho_factor = tf.reshape(
            self.rho(model(self.x)) / self.rho(self.x),
            (self.x.shape[0]),
        )
        expected_g_t = tf.multiply(
            tf.reshape(expected_rho_factor * (-1.5) * (-1.5), (self.x.shape[0], 1)),
            self.g(model(self.x)),
        )
        np.testing.assert_equal(transformed_estimations.numpy(), expected_g_t.numpy())

    def test_if_2d_g_outputs_a_2d_array_then_transformed_estimations_are_also_2d_arrays(
        self,
    ):
        model = get_two_dimensional_linear_model(m=-1.5, q=0.5)
        transformed_estimations = compute_transformed_estimations(model=model, rho=self.rho, g=self.g, x=self.x)

        expected_rho_factor = tf.reshape(
            self.rho(model(self.x)) / self.rho(self.x),
            (self.x.shape[0]),
        )
        expected_g_t = tf.multiply(
            tf.reshape(expected_rho_factor * (-1.5) * (-1.5), (self.x.shape[0], 1)),
            self.g(model(self.x)),
        )
        np.testing.assert_equal(transformed_estimations.numpy(), expected_g_t.numpy())
