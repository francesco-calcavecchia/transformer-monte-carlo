import tensorflow as tf


def get_one_dimensional_linear_model(m: float = 1.0, q: float = 0.0):
    return tf.keras.Sequential(
        [
            tf.keras.layers.Dense(
                units=1,
                activation="linear",
                input_shape=(1,),
                kernel_initializer=tf.constant_initializer(value=m),
                bias_initializer=tf.constant_initializer(value=q),
            )
        ]
    )


def get_two_dimensional_linear_model(m: float = 1.0, q: float = 0.0):
    return tf.keras.Sequential(
        [
            tf.keras.layers.Dense(
                units=2,
                activation="linear",
                input_shape=(2,),
                kernel_initializer=tf.keras.initializers.Identity(gain=m),
                bias_initializer=tf.constant_initializer(value=q),
            )
        ]
    )
